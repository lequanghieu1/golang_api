package middlewares

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	db "github.com/conglt10/web-golang/database"

	jwt "github.com/conglt10/web-golang/auth"
	res "github.com/conglt10/web-golang/utils"
	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
)

func CheckJwt(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		err := jwt.Verify(r)

		if err != nil {
			res.ERROR(w, 401, errors.New("Unauthorized"))
			return
		}

		next(w, r, ps)
	}
}
func CheckAdd(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var result bson.M
		username, err := jwt.ExtractUsernameFromToken(r)
		if err != nil {
			res.JSON(w, 500, "Internal Server Error")
			return
		}
		collections := db.ConnectRolesAccess()
		errFindRoles := collections.FindOne(context.TODO(), bson.M{"id_extra": username}).Decode(&result)
		add := fmt.Sprintf("%v", result["add"])
		if errFindRoles != nil {
			res.JSON(w, 409, "your account is not exits in roles-access")
			return
		}
		if errFindRoles == nil {
			if add == "false" {
				res.JSON(w, 409, " your account don't enough Permission")
				return
			}
		}
		next(w, r, ps)
	}
}
func CheckDelete(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		var result bson.M
		username, err := jwt.ExtractUsernameFromToken(r)
		if err != nil {
			res.JSON(w, 500, "Internal Server Error")
			return
		}
		collections := db.ConnectRolesAccess()
		errFindRoles := collections.FindOne(context.TODO(), bson.M{"id_extra": username}).Decode(&result)
		add := fmt.Sprintf("%v", result["delete"])
		if errFindRoles != nil {
			res.JSON(w, 409, "your account is not exits in roles-access")
			return
		}
		if errFindRoles == nil {
			if add == "false" {
				res.JSON(w, 409, " your account don't enough Permission")
				return
			}
		}
		next(w, r, ps)
	}
}
func EnableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
