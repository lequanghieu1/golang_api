package routes

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"net/smtp"
	"strconv"
	"time"

	"github.com/asaskevich/govalidator"
	jwt "github.com/conglt10/web-golang/auth"
	db "github.com/conglt10/web-golang/database"
	"github.com/conglt10/web-golang/middlewares"
	"github.com/conglt10/web-golang/models"
	res "github.com/conglt10/web-golang/utils"
	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/bson"
)

func Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	username := r.PostFormValue("username")
	password := r.PostFormValue("password")
	middlewares.EnableCors(&w)

	if govalidator.IsNull(username) || govalidator.IsNull(password) {
		res.JSON(w, 400, "Data can not empty")
		return
	}

	username = models.Santize(username)
	password = models.Santize(password)

	collection := db.ConnectUsers()

	var result bson.M
	err := collection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&result)

	if err != nil {
		res.JSON(w, 400, "Username or Password incorrect")
		return
	}

	// convert interface to string
	hashedPassword := fmt.Sprintf("%v", result["password"])

	err = models.CheckPasswordHash(hashedPassword, password)

	if err != nil {
		res.JSON(w, 401, "Username or Password incorrect")
		return
	}

	token, errCreate := jwt.Create(username)

	if errCreate != nil {
		res.JSON(w, 500, "Internal Server Error")
		return
	}

	res.JSON(w, 200, token)
}

func Register(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	username := r.PostFormValue("username")
	fullname := r.PostFormValue("fullname")
	password := r.PostFormValue("password")
	email := r.PostFormValue("email")
	middlewares.EnableCors(&w)

	if govalidator.IsNull(username) || govalidator.IsNull(email) || govalidator.IsNull(fullname) || govalidator.IsNull(password) {
		res.JSON(w, 400, "Data can not empty")
		return
	}
	if !govalidator.IsEmail(email) {
		res.JSON(w, 400, "Email is invalid")
		return
	}
	username = models.Santize(username)
	fullname = models.Santize(fullname)
	password = models.Santize(password)
	email = models.Santize(email)

	collection := db.ConnectUsers()
	var result bson.M
	errFindUsername := collection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&result)
	errFindEmail := collection.FindOne(context.TODO(), bson.M{"email": email}).Decode(&result)

	if errFindUsername == nil {
		res.JSON(w, 409, "User does exists")
		return
	}
	if errFindEmail == nil {
		res.JSON(w, 409, "Email does exists")
		return
	}

	password, err := models.Hash(password)

	if err != nil {
		res.JSON(w, 500, "Register has failed")
		return
	}

	newUser := bson.M{"username": username, "email": email, "fullname": fullname, "password": password, "is_login": false, "created_at": time.Now(), "updated_at": time.Now()}

	_, errs := collection.InsertOne(context.TODO(), newUser)

	if errs != nil {
		res.JSON(w, 500, "Register has failed")
		return
	}

	res.JSON(w, 201, "Register Succesfully")
}
func init() {

	rand.Seed(time.Now().UnixNano())
}
func sendEmail(w http.ResponseWriter, e string) {
	otp := rand.Intn(1000000)
	fromEmail := "thuymylevn@gmail.com"
	password := "thuymy01**"
	toEmail := e
	to := []string{toEmail}
	host := "smtp.gmail.com"
	port := "587"
	address := host + ":" + port
	message := []byte("Your otp is " + strconv.Itoa(otp))
	auth := smtp.PlainAuth("", fromEmail, password, host)

	err := smtp.SendMail(address, auth, fromEmail, to, message)

	if err != nil {
		fmt.Println("err:", err)
		return
	}
	otp = otp * 369
	res.JSON(w, 200, otp)

}
func ForgetPass(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	email := r.PostFormValue("email")
	middlewares.EnableCors(&w)
	if govalidator.IsNull(email) {
		res.JSON(w, 400, "Data can not empty")
		return
	}
	email = models.Santize(email)

	collection := db.ConnectUsers()
	var result bson.M
	errFindUsername := collection.FindOne(context.TODO(), bson.M{"email": email}).Decode(&result)

	if errFindUsername != nil {
		res.JSON(w, 409, "Email don't match any user")
		return
	}
	sendEmail(w, email)
}
func UpdatePass(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	password := r.PostFormValue("password")
	email := r.PostFormValue("email")
	middlewares.EnableCors(&w)

	if govalidator.IsNull(password) {
		res.JSON(w, 400, "Data can not empty")
		return
	}
	password = models.Santize(password)

	collection := db.ConnectUsers()
	password, err := models.Hash(password)

	if err != nil {
		res.JSON(w, 500, "Register has failed")
		return
	}
	filter := bson.M{"email": email}
	update := bson.M{"$set": bson.M{"password": password}}

	_, errUpdate := collection.UpdateOne(context.TODO(), filter, update)

	if errUpdate != nil {
		res.JSON(w, 500, "Reset password has failed")
		return
	}

	res.JSON(w, 200, "Reset password Successfully")
}
