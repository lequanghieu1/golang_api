package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/conglt10/web-golang/middlewares"
	"github.com/conglt10/web-golang/routes"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
)

func main() {

	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	}

	router := httprouter.New()

	router.POST("/login", routes.Login)
	router.POST("/register", routes.Register)
	router.POST("/forget-pass", routes.ForgetPass)
	router.POST("/update-pass", routes.UpdatePass)

	//__________________________Manager____________________________
	router.GET("/managers", routes.GetAllUsers)
	router.POST("/managers", middlewares.CheckJwt(middlewares.CheckAdd(routes.CreateManager)))
	router.PUT("/managers/:id", routes.EditManager)
	router.DELETE("/managers/:id", middlewares.CheckJwt(middlewares.CheckDelete(routes.DeleteManager)))
	//__________________________Event-Code____________________________
	router.GET("/event-code", routes.GetAllEvents)
	router.POST("/event-code", middlewares.CheckJwt(middlewares.CheckAdd(routes.CreateEventCode)))
	router.PUT("/event-code/:id", middlewares.CheckJwt(routes.EditEventCode))
	router.DELETE("/event-code/:id", middlewares.CheckJwt(middlewares.CheckDelete(routes.DeleteEventCode)))
	//__________________________Model-Device____________________________
	router.GET("/model-device", routes.GetAllModels)
	router.POST("/model-device", middlewares.CheckJwt(middlewares.CheckAdd(routes.CreateModelDevices)))
	router.PUT("/model-device/:id", middlewares.CheckJwt(routes.EditModelDevice))
	router.DELETE("/model-device/:id", middlewares.CheckJwt(middlewares.CheckDelete(routes.DeleteModelDevice)))
	//__________________________Page-Schema____________________________
	router.GET("/page-schema", routes.GetAllPages)
	router.POST("/page-schema", middlewares.CheckJwt(middlewares.CheckAdd(routes.CreatePageSchema)))
	router.PUT("/page-schema/:id", middlewares.CheckJwt(routes.EditPageSchema))
	router.DELETE("/page-schema/:id", middlewares.CheckJwt(middlewares.CheckDelete(routes.DeletePageSchema)))
	//__________________________Roles-Access____________________________
	router.GET("/roles-access", routes.GetAllRoles)
	router.POST("/roles-access", middlewares.CheckJwt(middlewares.CheckAdd(routes.CreateRolesAccess)))
	router.PUT("/roles-access/:id", middlewares.CheckJwt(routes.EditRolesAccess))
	router.DELETE("/roles-access/:id", middlewares.CheckJwt(middlewares.CheckDelete(routes.DeleteRolesAccess)))

	fmt.Println("Listening to port 4000")
	log.Fatal(http.ListenAndServe(":4000", router))
}
